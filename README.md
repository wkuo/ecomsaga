### Saga Transaction Demo
This version has issue with performance and incorrect reversal (it will execute all reverse transaction if called services response with exception)

## Areas to explore & improve after review
- try to tweak the connection pool size
- avoid logging
- exception can be throw right after findId not found (explore Optional), let the called handle exception
- explore on batch update/insert
- explore other features available in spring framework such as manage transaction via hibernate, usage of Transaction annotation and so on...
- docker-compose - can define networks, alternatively can use container_name + change host file to allow services to reach each other


# Note:
The attached docker-compose.yml was stored the root folder (that consist of ecomsaga, ecomwallet, ecominventory folders). Move in to this folder for check in purpose only (without create new repo). 





