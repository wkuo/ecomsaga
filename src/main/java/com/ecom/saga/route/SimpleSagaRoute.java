package com.ecom.saga.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.http.common.HttpMethods;
import org.apache.camel.impl.saga.InMemorySagaService;

import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import com.ecom.saga.model.Inventory;
import com.ecom.saga.model.Order;
import com.ecom.saga.model.Wallet;



@Component
public class SimpleSagaRoute extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		restConfiguration()
			.component("servlet").enableCORS(true);
		
		getContext().addService(new InMemorySagaService(), true);				
		
		rest().put("/payment")
			.type(Order.class)
			.route()
			.saga()
				.compensation("direct:cancelPayment")
				.option("order", simple("${body}"))
					.unmarshal().json(JsonLibrary.Jackson, Order.class)
					.multicast().parallelProcessing()
						.to("direct:doPayment")
						.to("direct:minusStock")
					.end()
					.marshal().json(JsonLibrary.Jackson)
			.end();
		
		from("direct:doPayment")
			.setBody().body(Order.class, this::getWallet)
			.marshal().json(JsonLibrary.Jackson)
			.serviceCall("wallet/wallet/deduct/${id}");
		from("direct:reversePayment")
			.setBody().body(Order.class, this::getWallet)
			.marshal().json(JsonLibrary.Jackson)
			.serviceCall("wallet/wallet/add/${id}");
		
		
		from("direct:minusStock")
			.setBody().body(Order.class, this::getInventory)
			.marshal().json(JsonLibrary.Jackson)
			.serviceCall("inventory/inventory/deduct/${id}");
		from("direct:reverseStock")
			.setBody().body(Order.class, this::getInventory)
			.marshal().json(JsonLibrary.Jackson)
			.serviceCall("inventory/inventory/add/${id}");

		from("direct:cancelPayment")
			.setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.PUT))
			.setBody(header("order")).convertBodyTo(String.class)
			.unmarshal().json(JsonLibrary.Jackson, Order.class)
			.multicast().parallelProcessing()
				.to("direct:reversePayment")
				.to("direct:reverseStock")
          ;
		
//		.end()
//        .setHeader(Exchange.HTTP_METHOD, constant(HttpMethods.POST))
		
	}
	
	
	private Wallet getWallet(Order o) {
		Wallet w = new Wallet();
		w.setId(o.getUserId()); 
		w.setAmount(o.getAmount());
		//System.out.println("getwallet " + w);
		return w;
	}
	
	private Inventory getInventory(Order o) {
		Inventory i = new Inventory();
		i.setId(o.getItemId());
		i.setQuantity(o.getQuantity());
		//System.out.println("getinventory " + i);
		return i;
	}
	
	
}
