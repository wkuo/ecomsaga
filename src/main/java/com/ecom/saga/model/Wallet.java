package com.ecom.saga.model;

public class Wallet {
	
	private String id;
	
	private Double amount;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	@Override
	public String toString() {
		return "Wallet{" +
				"id='" + id + "'," +
				"amount=" + amount + 
				"}";
	}

}
