package com.ecom.saga.model;

public class Inventory {
	
	private String id;
	
	private Integer quantity;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Inventory{" + 
				"id='" + id + "'," +
				"quantity=" + quantity +
				"}";
	}
}
