package com.ecom.saga.model;

public class Order {
	
	private String userId;
	
	private Double amount;
	
	private String itemId;
	
	private Integer quantity;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public String toString() {
		return "Order{" + 
				"userId='" + userId + "'," +
				"amount=" + amount + "," +
				"itemId='" + itemId + "'," +
				"quantity=" + quantity + "}"
				;
	}
	
	
}
